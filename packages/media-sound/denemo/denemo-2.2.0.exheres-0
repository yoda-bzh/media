# Copyright 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.gz ] freedesktop-desktop gtk-icon-cache

SUMMARY="A music notation editor that lets you rapidly enter notation for typesetting via LilyPond"
DESCRIPTION="
Denemo is a music notation program for Linux and Windows that lets you rapidly enter notation for
typesetting via the LilyPond music engraver. Music can be typed in at the PC-Keyboard, or played in
via MIDI controller, or input acoustically into a microphone plugged into your computer's soundcard.
Denemo itself does not engrave the music for printout - it uses LilyPond which generates beautiful
sheet music to the highest publishing standards. Denemo just displays the staffs in a slim and
efficient way, so you can enter and edit the music efficiently
"
HOMEPAGE="http://www.denemo.org"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    alsa
    doc
    fluidsynth [[ description = [ Required for instant playback of scores, especially when not using midi ] ]]
    gtk-doc
    jack
    portmidi [[ description = [ MIDI backend, interface to alsa sound card ] ]]
"

# 1 of 1 test needs a running X server
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext[>=0.18]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-lang/guile:2.0
        dev-libs/glib:2[>=2.21.0]
        dev-libs/libxml2:2.0[>=2.3.10]
        gnome-desktop/evince[>=3.0]
        gnome-desktop/gtksourceview:3.0[>=3.0]
        gnome-desktop/librsvg:2[>=2.0]
        media-libs/aubio[>=0.4.0]
        media-libs/fontconfig[>=2.2.0]
        media-libs/libsamplerate[>=0.1.2]
        media-libs/libsndfile[>=1.0]
        media-libs/portaudio[>=19]
        sci-libs/fftw[>=3.1.2]
        x11-libs/gtk+:3[>=3.0.0]
        alsa? ( sys-sound/alsa-lib[>=1.0.0] )
        fluidsynth? ( media-sound/fluidsynth[>=1.0.8] )
        jack? ( media-sound/jack-audio-connection-kit[>=0.102.0] )
        portmidi? ( media-libs/portmidi )
    run:
        media-sound/lilypond
"

# Some of the options below could possibly be options.
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-rubberband
    --disable-static
    --disable-gtk2
    --enable-guile_2_0
    --enable-aubio4
    --enable-gtk3
    --enable-x11
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    alsa
    doc
    fluidsynth
    jack
    gtk-doc
    portmidi
)

src_prepare() {
    # respect localedir
    # drop if po/Makefile.in.in doesn't have 'itlocaledir = $(prefix)/$(DATADIRNAME)/locale'
    edo intltoolize --force --automake

    default
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

