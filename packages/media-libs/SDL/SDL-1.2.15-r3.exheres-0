# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 ] ]

SUMMARY="The Simple DirectMedia Layer library"
HOMEPAGE="http://libsdl.org"
DOWNLOADS="${HOMEPAGE}/release/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="X aalib dga pulseaudio"

DEPENDENCIES="
    build+run:
        sys-sound/alsa-lib
        X? (
            x11-dri/glu
            x11-dri/mesa
            x11-libs/libICE
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXinerama
            x11-libs/libXrandr
            x11-libs/libXrender
            x11-libs/libXt
            x11-libs/libXv
        )
        aalib? ( media-libs/aalib )
        pulseaudio? ( media-sound/pulseaudio )
"

AT_M4DIR=( /usr/share/aclocal acinclude )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.2.15-91ad7b43317a.patch
    "${FILES}"/${PN}-1.2.15-pkg-config.patch
)
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-alsa
    --disable-arts
    --disable-esd
    --disable-nas
    --disable-oss
    --disable-rpath
    --disable-video-caca
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( "X x" )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "X video-opengl"
    "X video-x11-xinerama"
    "X video-x11-xv"
    "aalib video-aalib"
    dga
    pulseaudio
)

