# Copyright 2016-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=manisandro release=v${PV} suffix=tar.xz ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Front-end to tesseract-ocr"
DESCRIPTION="
gImageReader is a simple front-end to tesseract. Features include:
 - Automatic page layout detection
 - User can manually define and adjust recognition regions
 - Import images from disk, scanning devices, clipboard and screenshots
 - Supports multipage PDF documents
 - Recognized text displayed directly next to the image
 - Editing of output text, including search/replace and removing line breaks
 - Spellchecking for output text (if corresponding dictionary installed)
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/libzip
        app-arch/quazip
        app-spell/enchant:2
        app-text/djvu
        app-text/podofo:=[>=0.9.3]
        app-text/poppler[qt5]
        app-text/qtspell[>=0.8.0]
        app-text/tesseract
        media-gfx/sane-backends
        sys-libs/libgomp:=
        x11-libs/qtbase:5[gui]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.4.2-Use-CMAKE_INSTALL_FULL_DATAROOTDIR.patch
    "${FILES}"/${PN}-3.2.3-fix-localesearch.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DINTERFACE_TYPE:STRING=qt5
    -DMANUAL_DIR:PATH=/usr/share/doc/${PNVR}
    -DENABLE_VERSIONCHECK:BOOL=FALSE
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

