# Copyright 2015 Jonathan Dahan <jonathan@jonathan.is>
# Distributed under the terms of the GNU General Public License v2

# https://github.com/obsproject/obs-browser/commits/master
OBS_BROWSER_VERSION="996b5a7bc43d912f1f4992e0032d4f263ac8b060"
# https://github.com/obsproject/obs-studio/blob/master/build-aux/modules/99-cef.json#L24
LINUX_CEF_BUILD_VERSION="5060"
# https://github.com/obsproject/obs-websocket/commits/master
OBS_WEBSOCKET_VERSION="d5077fca03a47144f7c0eb81b5d3278186e31d59"

require github [ user=obsproject ] cmake freedesktop-desktop gtk-icon-cache
require ffmpeg [ options="[h264][vorbis]" ]

export_exlib_phases src_prepare src_configure src_install pkg_postinst pkg_postrm

SUMMARY="Open source broadcasting software for live streaming and recording"
HOMEPAGE+=" https://obsproject.com"
DOWNLOADS+="
    https://github.com/obsproject/obs-browser/archive/${OBS_BROWSER_VERSION}.tar.gz -> obs-browser-${OBS_BROWSER_VERSION}.tar.gz
    https://github.com/obsproject/obs-websocket/archive/${OBS_WEBSOCKET_VERSION}.tar.gz -> obs-websocket-${OBS_WEBSOCKET_VERSION}.tar.gz
    browser-plugin? (
        https://cdn-fastly.obsproject.com/downloads/cef_binary_${LINUX_CEF_BUILD_VERSION}_linux_x86_64_v3.tar.xz
    )
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    alsa
    browser-plugin [[ description = [ Support for embedding websites, e.g. for stream alerts ] ]]
    fdk-aac [[ description = [ Support for AAC encoding using the Frauenhofer AAC Codec Library ] ]]
    jack
    pipewire [[ description = [ Suppport for capturing screen using PipeWire ] ]]
    pulseaudio
    scripting [[ description = [ Support for Lua and Python scripts ] ]]
    speex [[ description = [ Support for speexdsp-based noise suppression (Noise Gate) filter ] ]]
    v4l
    vlc [[ description = [ Support adding video playlists via libvlc ] ]]
    wayland [[ requires = pipewire ]]
    webrtc [[ description = [ Support streaming to WebRTC (WHIP) endpoints ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        browser-plugin? ( dev-libs/json )
    build+run:
        dev-libs/glib:2
        dev-libs/jansson[>=2.5]
        dev-libs/libglvnd
        dev-libs/mbedtls
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/rnnoise
        media-libs/x264:=
        net-misc/curl
        sys-apps/pciutils
        sys-apps/util-linux
        sys-libs/zlib
        x11-libs/libva
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXfixes
        x11-libs/libxcb
        x11-libs/qtbase:6[gui]
        x11-libs/qtsvg:6
        alsa? ( sys-sound/alsa-lib )
        browser-plugin? (
            dev-libs/at-spi2-atk
            dev-libs/at-spi2-core
            dev-libs/atk
            dev-libs/expat
            dev-libs/nspr
            dev-libs/nss
            net-print/cups
            sys-apps/dbus
            sys-sound/alsa-lib
            x11-dri/libdrm
            x11-dri/mesa
            x11-libs/cairo
            x11-libs/pango
            x11-libs/libXext
            x11-libs/libXdamage
            x11-libs/libxkbcommon
            x11-libs/libXrandr
            x11-libs/libxshmfence
        )
        fdk-aac? ( media-libs/fdk-aac )
        jack? ( media-sound/jack-audio-connection-kit )
        pipewire? (
            media/pipewire[>=0.3.33]
            x11-dri/libdrm
        )
        pulseaudio? ( media-sound/pulseaudio )
        scripting? (
            dev-lang/LuaJIT
            dev-lang/python:*[>=3.4]
            dev-lang/swig
        )
        speex? ( media-libs/speexdsp )
        v4l? (
            media-libs/v4l-utils
            providers:eudev? ( sys-apps/eudev )
            providers:systemd? ( sys-apps/systemd )
        )
        vlc? ( media/vlc )
        wayland? ( sys-libs/wayland )
        webrtc? ( net-libs/libdatachannel[>=0.20] )
        !media-video/obs-studio[<28.0.0] [[
            description = [ Previous installations of obs-studio have to be uninstalled manually first due to installing a symlink now ]
            resolution = manual
        ]]
    run:
        pipewire? ( sys-apps/xdg-desktop-portal )
    suggestion:
        media-video/v4l2loopback [[
            description = [ Support for virtual camera output ]
        ]]
"

obs-studio_src_prepare() {
    cmake_src_prepare

    # obs-browser
    edo rmdir "${CMAKE_SOURCE}"/plugins/obs-browser
    edo ln -s "${WORKBASE}"/obs-browser-${OBS_BROWSER_VERSION} "${CMAKE_SOURCE}"/plugins/obs-browser

    # obs-websocket
    edo rmdir "${CMAKE_SOURCE}"/plugins/obs-websocket
    edo ln -s "${WORKBASE}"/obs-websocket-${OBS_WEBSOCKET_VERSION} "${CMAKE_SOURCE}"/plugins/obs-websocket
}

obs-studio_src_configure() {
    local cmakeparams=(
        -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
        -DBUILD_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DCALM_DEPRECATION:BOOL=TRUE
        -DCCACHE_SUPPORT:BOOL=FALSE
        -DCEF_ROOT_DIR:PATH="${WORKBASE}"/cef_binary_${LINUX_CEF_BUILD_VERSION}_linux_x86_64
        -DENABLE_AJA:BOOL=FALSE
        -DENABLE_ALSA:BOOL=$(option alsa TRUE FALSE)
        -DENABLE_BROWSER:BOOL=$(option browser-plugin TRUE FALSE)
        -DENABLE_DECKLINK:BOOL=FALSE
        -DENABLE_LIBFDK:BOOL=$(option fdk-aac TRUE FALSE)
        -DENABLE_FFMPEG_LOGGING:BOOL=FALSE
        -DENABLE_FFMPEG_MUX_DEBUG:BOOL=FALSE
        -DENABLE_FREETYPE:BOOL=TRUE
        -DENABLE_HEVC:BOOL=TRUE
        -DENABLE_JACK:BOOL=$(option jack TRUE FALSE)
        -DENABLE_NEW_MPEGTS_OUTPUT:BOOL=FALSE
        -DENABLE_PIPEWIRE:BOOL=$(option pipewire TRUE FALSE)
        -DENABLE_PLUGINS:BOOL=TRUE
        -DENABLE_PULSEAUDIO:BOOL=$(option pulseaudio TRUE FALSE)
        -DENABLE_QSV11:BOOL=FALSE
        -DENABLE_RTMPS:STRING=ON
        -DENABLE_SCRIPTING:BOOL=$(option scripting TRUE FALSE)
        -DENABLE_SCRIPTING_LUA:BOOL=$(option scripting TRUE FALSE)
        -DENABLE_SCRIPTING_PYTHON:BOOL=$(option scripting TRUE FALSE)
        -DENABLE_SERVICE_UPDATES:BOOL=FALSE
        -DENABLE_SNDIO:BOOL=FALSE
        -DENABLE_SPEEXDSP:BOOL=$(option speex TRUE FALSE)
        -DENABLE_STATIC_MBEDTLS:BOOL=FALSE
        -DENABLE_UDEV:BOOL=$(option v4l TRUE FALSE)
        -DENABLE_UI:BOOL=TRUE
        -DENABLE_V4L2:BOOL=$(option v4l TRUE FALSE)
        -DENABLE_VLC:BOOL=$(option vlc TRUE FALSE)
        -DENABLE_VST:BOOL=FALSE
        -DENABLE_WAYLAND:BOOL=$(option wayland TRUE FALSE)
        -DENABLE_WEBRTC:BOOL=$(option webrtc TRUE FALSE)
        -DENABLE_WEBSOCKET:BOOL=FALSE
        -DENABLE_WHATSNEW:BOOL=FALSE
        -DINSTALLER_RUN:BOOL=FALSE
        -DOBS_VERSION_OVERRIDE:STRING=${PV}
        -DUNIX_STRUCTURE:BOOL=TRUE
        -DUSE_XDG:BOOL=TRUE
        # https://github.com/obsproject/obs-studio/issues/6928
        -DCMAKE_INSTALL_DATAROOTDIR:PATH=share
)

        ecmake "${cmakeparams[@]}"
}

obs-studio_src_install() {
    cmake_src_install

    # https://github.com/obsproject/obs-studio/issues/6928
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/{applications,icons,metainfo,obs} "${IMAGE}"/usr/share
    dosym /usr/share/obs /usr/$(exhost --target)/share/obs
}

obs-studio_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

obs-studio_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

