# Copyright 2015 Xavier Barrachina <xv.barrachina@gmail.com>
# Copyright 2009, 2010 Michael Forney
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop freedesktop-mime gtk-icon-cache
require option-renames [ renames=[ 'qt5 gui' ] ]

export_exlib_phases src_compile src_test src_install pkg_postinst pkg_postrm

SUMMARY="MKVToolnix is a set of tools to create, alter and inspect Matroska files under Linux"
HOMEPAGE="https://${PN}.download"
DOWNLOADS="${HOMEPAGE}/sources/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    dvdread [[ description = [ Support for reading chapters from a DVD folder structure ] ]]
    flac
    gui
    ( providers: qt5 qt6 ) [[ number-selected = exactly-one ]]
"

QT5_MIN_VER="5.9.0"
QT6_MIN_VER="6.1.0"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        app-text/po4a
        dev-lang/ruby:*[>=2.0.0]
        dev-libs/json[>=3.5.0]
        dev-libs/libxslt
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/boost[>=1.66.0]
        dev-libs/fmt[>=6.1.0]
        dev-libs/gmp:=
        dev-libs/pugixml[>=1.9]
        media-libs/libebml[>=1.4.4]
        media-libs/libmatroska[>=1.7.1]
        media-libs/libogg
        media-libs/libvorbis
        sys-libs/zlib
        dvdread? ( media-libs/libdvdread )
        flac? ( media-libs/flac:= )
        gui? (
            app-text/cmark:=
            providers:qt5? (
                x11-libs/qtbase:5[>=${QT5_MIN_VER}][gui]
                x11-libs/qtmultimedia:5[>=${QT5_MIN_VER}]
                x11-libs/qtsvg:5[>=${QT5_MIN_VER}]
            )
            providers:qt6? (
                x11-libs/qtbase:6[>=${QT6_MIN_VER}][gui]
                x11-libs/qtmultimedia:6[>=${QT6_MIN_VER}]
                x11-libs/qtsvg:6[>=${QT6_MIN_VER}]
            )
        )
        providers:qt5? ( x11-libs/qtbase:5[>=${QT5_MIN_VER}] )
        providers:qt6? ( x11-libs/qtbase:6[>=${QT6_MIN_VER}] )
    test:
        dev-cpp/gtest[>=1.8.0]
    suggestion:
        media/mediainfo[gui] [[
            description = [ Support MediaInfo for displaying additional information ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-mkvtoolnix
    --disable-optimization
    --disable-static
    --disable-update-check
    --with-gettext
    --with-qt-pkg-config
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    gui
    "providers:qt5 qt5"
    "providers:qt6 qt6"
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    dvdread
    flac
)

mkvtoolnix_src_compile() {
    edo rake V=1 -m -j${EXJOBS:-1}
}

mkvtoolnix_src_test() {
    edo rake V=1 -m -j${EXJOBS:-1} tests:run_unit
}

mkvtoolnix_src_install() {
    edo rake DESTDIR="${IMAGE}" install
}

mkvtoolnix_pkg_postinst() {
    option gui && freedesktop-desktop_pkg_postinst
    option gui && freedesktop-mime_pkg_postinst
    option gui && gtk-icon-cache_pkg_postinst
}

mkvtoolnix_pkg_postrm() {
    option gui && freedesktop-desktop_pkg_postrm
    option gui && freedesktop-mime_pkg_postrm
    option gui && gtk-icon-cache_pkg_postrm
}

