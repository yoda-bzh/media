# Copyright 2008 Richard Brown
# Copyright 2010 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2011,2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache
require meson
require vala [ vala_dep=true with_opt=true ]

export_exlib_phases src_{prepare,configure,install} pkg_{postinst,postrm}

SUMMARY="GNU Image Manipulation Program"
HOMEPAGE="https://www.gimp.org"
DOWNLOADS="https://download.gimp.org/pub/gimp/v$(ever range 1-2)/${PNV}.tar.xz"

LICENCES="GPL-3 LGPL-3"
SLOT="0"
MYOPTIONS="
    aalib
    alsa
    backtrace [[ description = [ Support for detailed backtraces via dev-libs/libunwind ] ]]
    fits [[ description = [ Support for the F(lexible)I(mage)T(ransport)S(ystem) format ] ]]
    gtk-doc
    hdr [[ description = [ Support for the OpenEXR image file format ] ]]
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    javascript [[ description = [ Install JavaScript plug-ins ] ]]
    jpeg2000
    jpegxl [[ description = [ Support for the JPEG XL image file format ] ]]
    lua [[ description = [ Install Lua plug-ins ] ]]
    mng
    openmp [[ description = [ Support for Open Multi-Processing ] ]]
    postscript [[ description = [ Support importing PS (PostScript) files ] ]]
    python
    webp [[ description = [ Support for the Webp image format ] ]]
    wmf [[ description = [ Support for the Windows Metafile image format ] ]]

    platform: amd64
    x86_cpu_features: mmx sse
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# tests attempt to use X and fail
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.10.0]
        sys-devel/gettext[>=0.19]
        virtual/pkg-config[>=0.16]
        gtk-doc? ( dev-doc/gi-docgen )
    build+run:
        app-arch/libarchive
        app-arch/bzip2
        app-arch/xz[>=5.0.0]
        app-text/iso-codes
        app-text/poppler[>=0.69.0][cairo][glib]
        app-text/poppler-data[>=0.4.9]
        core/json-glib[>=1.2.6]
        dev-libs/appstream-glib[>=0.7.7]
        dev-libs/atk[>=2.4.0]
        dev-libs/gexiv2[>=0.14.0]
        dev-libs/glib:2[>=2.68.0]
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        gnome-desktop/gobject-introspection:1[>=1.32.0]
        gnome-desktop/libgudev[>=167]
        gnome-desktop/librsvg:2[>=2.40.6]
        graphics/mypaint-brushes:0[<2]
        media-libs/babl[>=0.1.98][gobject-introspection][vapi]
        media-libs/fontconfig[>=2.12.4]
        media-libs/freetype:2[>=2.1.7]
        media-libs/gegl:0.4[>=0.4.48][gobject-introspection][vapi]
        media-libs/tiff:=[>=4.0.0]
        media-libs/lcms2[>=2.8]
        media-libs/libmypaint:0[>=1.4.0]
        media-libs/libpng:=[>=1.6.25]
        sys-libs/zlib
        x11-libs/cairo[>=1.14.0] [[ note = [ needs cairo-pdf, which we have enabled by default ] ]]
        x11-libs/gdk-pixbuf:2.0[>=2.30.8]
        x11-libs/gtk+:3[>=3.22.29][gobject-introspection]
        x11-libs/harfbuzz[>=1.0.5]
        x11-libs/libX11
        x11-libs/libXcursor [[ note = [ hard-enabled because gtk+ needs it anyway ] ]]
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXmu
        x11-libs/libXpm [[ note = [ hard-enabled because people who have xorg-server or xterm have it installed anyway and it's small ] ]]
        x11-libs/pango[>=1.50.0]
        aalib? ( media-libs/aalib )
        alsa? ( sys-sound/alsa-lib[>=1.0.0] )
        backtrace? ( dev-libs/libunwind[>=1.1.0] )
        fits? ( sci-libs/cfitsio )
        hdr? ( media-libs/openexr[>=1.6.1] )
        heif? ( media-libs/libheif[>=1.15.1] )
        javascript? ( gnome-bindings/gjs:1 )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.1.0] )
        jpegxl? ( media-libs/libjxl:=[>=0.7.0] )
        lua? ( dev-lang/LuaJIT )
        mng? ( media-libs/libmng )
        openmp? ( sys-libs/libgomp:= )
        postscript? ( app-text/ghostscript )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        python? (
            dev-lang/python:*[>=3.6]
            gnome-bindings/pygobject:3[>=3.0]
        ) [[ note = [ needs to be tested with python3 ] ]]
        webp? ( media-libs/libwebp:=[>=0.6.0] )
        wmf? ( media-libs/libwmf[>=0.2.8] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=167] )
    run:
        gnome-desktop/adwaita-icon-theme [[ note = [ assertion image-missing not present in theme Symbolic ] ]]
        x11-themes/hicolor-icon-theme
    test:
        dev-libs/appstream:*[>=0.15.3] [[ note = [ appstream-cli ] ]]
    suggestion:
        gnome-desktop/gvfs [[ description = [ Use gvfs to access remote files using the URI plugin ] ]]
"

gimp_src_prepare() {
    meson_src_prepare

    # TODO: fix upstream
    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)&:" tools/gimptool.c

}

gimp_src_configure() {
    local myconf=(
        -Dcairo-pdf=enabled
        -Dcheck-update=no
        -Denable-default-bin=enabled
        -Dg-ir-doc=false
        -Dgudev=enabled
        # This could maybe be used to enable tests, if xvfb was packaged
        -Dheadless-tests=disabled
        -Dilbm=disabled
        # Alternative to libunwind in order to get detailed traces
        -Dlibbacktrace=false
        -Dlinux-input=enabled
        -Dprint=true
        -Dwebkit-unmaintained=false
        -Dxcursor=enabled
        -Dxpm=enabled

        $(meson_feature aalib aa)
        $(meson_feature alsa)
        $(meson_feature fits)
        $(meson_feature gtk-doc gi-docgen)
        $(meson_feature hdr openexr)
        $(meson_feature heif)
        $(meson_feature javascript)
        $(meson_feature jpeg2000)
        $(meson_feature jpegxl jpeg-xl)
        $(meson_feature lua)
        $(meson_feature mng)
        $(meson_feature openmp)
        $(meson_feature postscript ghostscript)
        $(meson_feature python)
        $(meson_feature vapi vala)
        $(meson_feature webp)
        $(meson_feature wmf)

        $(meson_switch backtrace libunwind)

        $(expecting_tests '-Dappdata-test=enabled' '-Dappdata-test=disabled')
    )

    exmeson "${myconf[@]}"
}

gimp_src_install() {
    meson_src_install

    keepdir /usr/share/gimp/2.99/fonts
}

gimp_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

gimp_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

