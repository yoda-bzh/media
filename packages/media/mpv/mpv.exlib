# Copyright 2012-2015 Johannes Nixdorf <mixi@exherbo.org>
# Based in part upon 'mplayer2.exlib', which is
#   Copyright 2011 Elias Pipping <pipping@exherbo.org>
#   Copyright 2011 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# last checked revision: 517489814d6bc4837a1c24f7601a77136564fb7a

require freedesktop-desktop github [ user="mpv-player" tag="v${PV}" ] gtk-icon-cache meson \
        lua [ multibuild=false whitelist="5.1 5.2" with_opt=true ] bash-completion zsh-completion \
        option-renames [ renames=[ 'icc lcms' 'va vaapi' ] ] \
        ffmpeg [ abis=[ 6 7 ] options="[vaapi?][vdpau?]" ]

export_exlib_phases src_prepare src_configure src_install pkg_postrm pkg_postinst

SUMMARY="Video player based on MPlayer/mplayer2"
HOMEPAGE="https://mpv.io"

UPSTREAM_RELEASE_NOTES="https://github.com/mpv-player/mpv/releases"

LICENCES="GPL-2 GPL-3 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    alsa
    archive     [[ description = [ Adds support for reading media from zip files and more ] ]]
    bluray      [[ description = [ Adds support for video blurays ] ]]
    cd          [[ description = [ Adds support for audio CDs ] ]]
    cuda        [[ description = [ Adds support for NVIDIA hardware accelerated decoding using the CUDA API ] ]]
    dvd
    lcms        [[ description = [ Adds support for using ICC profiles through lcms2 ] ]]
    jack        [[ description = [ Support for the Jack Audio Connection Kit ] ]]
    lua         [[ description = [ Adds lua scripting support and an onscreen controller ] ]]
    pipewire
    pulseaudio
    rubberband  [[ description = [ Filter for time-stretching and pitch-shifting using librubberband ] ]]
    vaapi       [[ description = [ Adds support for decoding and presenting video using the Video Acceleration API ] ]]
    vapoursynth [[ description = [ Adds support for vapoursynth filters ] ]]
    vdpau       [[ description = [ Adds support for presenting and decoding video using the VDPAU API (-vo=vdpau and -hwdec=vdpau) ] ]]
    vulkan      [[ description = [ Support for the Vulkan API ] ]]
    wayland
    X
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-python/docutils
        virtual/pkg-config
        X? ( x11-proto/xorgproto )
        cuda? ( media-libs/nv-codec-headers[>=11.1.5.1] )
        vulkan? ( sys-libs/vulkan-headers[>=1.3.238] )
    build+run:
        dev-libs/libglvnd[>=1.4.0]
        media-libs/libass[fontconfig][>=0.12.1]
        media-libs/libplacebo:=[>=6.338.2][lcms?]
        sys-libs/ncurses
        sys-libs/zlib
        x11-dri/libdrm[>=2.4.105]
        x11-dri/mesa[>=17.1.0][X?][wayland?]
        alsa? ( sys-sound/alsa-lib[>=1.0.18] )
        archive? ( app-arch/libarchive[>=3.4.0] )
        bluray? ( media-libs/libbluray[>=0.3.0] )
        cd? (
            dev-libs/libcdio[>=0.90]
            dev-libs/libcdio-paranoia
        )
        dvd? (
            media-libs/libdvdnav[>=4.2.0]
            media-libs/libdvdread[>=4.1.0]
        )
        jack? ( media-sound/jack-audio-connection-kit[>=0.120.1] )
        lcms? ( media-libs/lcms2[>=2.6] )
        pipewire? ( media/pipewire[>=0.3.48] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        pulseaudio? ( media-sound/pulseaudio[>=1.0] )
        rubberband? ( media-libs/rubberband[>=1.8.0] )
        vapoursynth? ( media-libs/vapoursynth[>=26][python] )
        vaapi? ( x11-libs/libva[>=2.1.0][X?][wayland?] )
        vdpau? ( x11-libs/libvdpau[>=0.2] )
        vulkan? ( sys-libs/vulkan-loader[>=1.1.70] )
        wayland? (
            sys-libs/wayland[>=1.20.0]
            sys-libs/wayland-protocols[>=1.25]
            x11-libs/libxkbcommon[>=0.3.0]
        )
        X? (
            x11-libs/libX11[>=1.0.0]
            x11-libs/libXScrnSaver[>=1.0.0]
            x11-libs/libXext[>=1.0.0]
            x11-libs/libXpresent[>=1.0.0]
            x11-libs/libXrandr[>=1.4.0]
            x11-libs/libXv
        )
    run:
        cuda? ( $(FFMPEG_OPTIONS="[nvenc]" print_ffmpeg_dependencies) )
    suggestion:
        lua? ( net-misc/youtube-dl[>=2015.02.23.1] [[ description = [ Support to play videos from YouTube and other video sites ] ]] )
        lua? ( net-misc/yt-dlp [[ description = [ Support to play videos from YouTube and other video sites ] ]] )
"

mpv_src_prepare() {
    meson_src_prepare

    # fix docdir
    edo sed \
        -e "s:'doc', 'mpv':'doc', '${PNVR}':g" \
        -i meson.build
}

mpv_src_configure() {
    local meson_args=(
        -Dandroid-media-ndk=disabled
        -Daudiounit=disabled
        -Davfoundation=disabled
        -Dbuild-date=false
        -Dcaca=disabled
        -Dcocoa=disabled
        -Dcoreaudio=disabled
        -Dcplayer=true
        -Dcplugins=enabled
        -Dd3d-hwaccel=disabled
        -Dd3d11=disabled
        -Dd3d9-hwaccel=disabled
        -Ddirect3d=disabled
        -Ddrm=enabled
        -Ddvbin=enabled
        -Degl=enabled
        -Degl-android=disabled
        -Degl-angle=disabled
        -Degl-angle-lib=disabled
        -Degl-angle-win32=disabled
        -Degl-drm=enabled
        -Dgbm=enabled
        -Dgl=enabled
        -Dgl-cocoa=disabled
        -Dgl-dxinterop=disabled
        -Dgl-dxinterop-d3d9=disabled
        -Dgl-win32=disabled
        -Dgpl=true
        -Dhtml-build=disabled
        -Diconv=enabled
        -Dios-gl=disabled
        -Djavascript=disabled
        -Djpeg=enabled
        -Dlibavdevice=enabled
        -Dlibmpv=true
        -Dmacos-cocoa-cb=disabled
        -Dmacos-media-player=disabled
        -Dmacos-touchbar=disabled
        -Dmanpage-build=enabled
        -Dopenal=disabled
        -Dopensles=disabled
        -Doss-audio=disabled
        -Dpdf-build=disabled
        -Dplain-gl=enabled
        -Dpthread-debug=disabled
        -Dsdl2=disabled
        -Dsdl2-audio=disabled
        -Dsdl2-gamepad=disabled
        -Dsdl2-video=disabled
        -Dshaderc=disabled
        -Dsixel=disabled
        -Dsndio=disabled
        -Dspirv-cross=disabled
        -Dswift-build=disabled
        -Dswift-flags=""
        -Dta-leak-report=false
        -Duchardet=disabled
        -Duwp=disabled
        -Dvaapi-win32=disabled
        -Dvector=disabled
        -Dvideotoolbox-gl=disabled
        -Dvideotoolbox-pl=disabled
        -Dwasapi=disabled
        -Dwin32-threads=disabled
        -Dzimg=disabled
        -Dzlib=enabled
        $(meson_feature alsa)
        $(meson_feature archive libarchive)
        $(meson_feature bluray libbluray)
        $(meson_feature cd cdda)
        $(meson_feature cuda cuda-hwaccel)
        $(meson_feature cuda cuda-interop)
        $(meson_feature dvd dvdnav)
        $(meson_feature jack)
        $(meson_feature lcms lcms2)
        $(meson_feature pipewire)
        $(meson_feature pulseaudio pulse)
        $(meson_feature rubberband)
        $(meson_feature vaapi)
        $(meson_feature vaapi vaapi-drm)
        $(if option wayland; then
            meson_feature vaapi vaapi-wayland
        fi)
        $(if option X; then
            meson_feature vaapi vaapi-x11
        fi)
        $(meson_feature vapoursynth)
        $(meson_feature vdpau)
        $(if option X; then
            meson_feature vdpau vdpau-gl-x11
        fi)
        $(meson_feature vulkan)
        $(meson_feature vulkan vulkan-interop)
        $(meson_feature wayland)
        $(meson_feature wayland dmabuf-wayland)
        $(meson_feature wayland egl-wayland)
        $(meson_feature X egl-x11)
        $(meson_feature X gl-x11)
        $(meson_feature X x11)
        $(meson_feature X xv)
        $(option lua -Dlua=lua-${LUA_ABIS} -Dlua=disabled)
        $(expecting_tests -Dtests=true -Dtests=false)
    )

    exmeson "${meson_args[@]}"
}

mpv_src_install() {
    meson_src_install

    if option !bash-completion; then
        # Completion file is installed unconditionally it seems
        edo rm -fr "${IMAGE}"/usr/share/bash-completion
    fi

    if option !zsh-completion; then
        # Completion file is installed unconditionally it seems
        edo rm -fr "${IMAGE}"/usr/share/zsh
    fi
}

mpv_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

mpv_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

